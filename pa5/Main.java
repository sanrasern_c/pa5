package pa5;

import java.util.Timer;
import java.util.TimerTask;
/**
 * This is main to run the program.
 * @author Sanrasern 5710547247
 */
public class Main {
	public static void main(String [] args) {
		Clock clock = new Clock();
		GUI gui = new GUI();
		clock.addObserver(gui);
		
		TimerTask clocktask = new ClockTask( clock );
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(clocktask, 1000, 1000);
		
	}
}
