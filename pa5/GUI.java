package pa5;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.net.URL;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;
/**
 * This is a GUI of this program.
 * @author Sanrasern 5710547247
 *
 */
public class GUI extends JFrame implements Observer {
	
	/**Initial variables.*/
	private JButton set, plus, minus;
	JLabel hour1 , hour2 , min1 , min2 , sec1 , sec2 , dot1 , dot2;
	Clock clock;
	boolean check = true;
	
	/**Constructor of this class.*/
	public GUI() {
		initComponents();
	}
	
	/**This is initComponents.*/
	public void initComponents() {
		
		/*Name of the title bar.*/
		super.setTitle("Cheap");
		/*Create JPanel.*/
		JPanel pane = new JPanel();
		/**Order the Panel in BoxLayout in Y-axis.*/
		pane.setLayout( new BoxLayout( pane , BoxLayout.Y_AXIS ) );
		/*Create another Panel.*/
		JPanel flow1 = new JPanel();
		flow1.setLayout( new FlowLayout() );
		
		/*Initial Components.*/
		hour1 = new JLabel("0");
		hour2 = new JLabel("0");
		min1 = new JLabel("0");
		min2 = new JLabel("0");
		sec1 = new JLabel("0");
		sec2 = new JLabel("0");
		dot1 = new JLabel(":");
		dot2 = new JLabel(":");
		
		/*Add Components in to Panel.*/
		flow1.add(hour1);
		flow1.add(hour2);
		flow1.add(dot1);
		flow1.add(min1);
		flow1.add(min2);
		flow1.add(dot2);
		flow1.add(sec1);
		flow1.add(sec2);
		
		/*Add Panel in to another Panel.*/
		pane.add(flow1);
		
		/*Create JPanel.*/
		JPanel flow2 = new JPanel();
		flow2.setLayout( new FlowLayout() );
		
		/* Initial Components and Initial event to Button */
		set = new JButton("SET");
		set.addActionListener(new Set());
		
		minus = new JButton("-");
		minus.addMouseListener(new Minus());
		
		plus = new JButton("+");
		plus.addMouseListener(new Plus());

		/*Add Components in to Panel.*/
		flow2.add(set);
		flow2.add(minus);
		flow2.add(plus);
		pane.add(flow2);
		this.add(pane);
		
		this.setVisible(true);
		this.pack();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	


	@Override
	/**
	 * To update the time.
	 */
	public void update(Observable clock, Object time) {
		Date newDate = (Date)time;
		this.clock = (Clock)clock;
		 
		/* Set the text for appear the time. */
		hour1.setText(newDate.getHours()/10+"");
		hour2.setText(newDate.getHours()%10+"");
		min1.setText(newDate.getMinutes()/10+"");
		min2.setText(newDate.getMinutes()%10+"");
		sec1.setText(newDate.getSeconds()/10+"");
		sec2.setText(newDate.getSeconds()%10+"");
	}
	
	/** Abstract class of Set button .*/
	class Set implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			clock.state.hitSet(GUI.this,clock);
		}
	}
	
	/** Abstract class of Plus button .*/
	class Plus extends MouseAdapter {
		@Override
		public void mousePressed(MouseEvent e) {
			clock.state.hitPlus(clock);	
		}
		
		public void mouseReleased(MouseEvent e) {
			/*Set to show current time after released mouse from plus button in DispayTime State.*/
			if(clock.state.getClass() == DisplayTime.class)
			clock.time = clock.now;
		}
	}
	
	/** Abstract class of Minus button .*/
	class Minus extends MouseAdapter {
		@Override
		public void mousePressed(MouseEvent e) {
			clock.state.hitMinus(clock);
		}
		
		public void mouseReleased(MouseEvent e) {
			/* Set value of the boolean check. */
			if(clock.check == true) clock.check = false;
			else clock.check = true;
		}
	}
	
}
