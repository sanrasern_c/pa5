package pa5;

import java.awt.Color;

public class SetSecs implements State {

	@Override
	public void hitSet(GUI gui , Clock clock) {
		/* Go to displayTime state. */
		clock.setState(clock.displayTime);
		clock.time = clock.now;
		
		/* Set color of the text. */
		gui.sec1.setForeground(Color.BLACK);
		gui.sec2.setForeground(Color.BLACK);
		
	}

	@Override
	public void hitMinus(Clock clock) {
		/* Set to edit alarm time. */
		clock.alarmTime.setSeconds(Math.floorMod(clock.alarmTime.getSeconds() - 1 , 60 ));
		
	}

	@Override
	public void hitPlus(Clock clock) {
		/* Set to edit alarm time. */
		clock.alarmTime.setSeconds(Math.floorMod(clock.alarmTime.getSeconds() + 1 , 60 ));
		
	}

	

}
