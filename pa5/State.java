package pa5;
/**
 * This is a interface of State in this program.
 * @author Sanrasern 5710547247
 *
 */
public interface State {
	public void hitSet(GUI gui,Clock clock);
	public void hitMinus(Clock clock);
	public void hitPlus(Clock clock);
}
