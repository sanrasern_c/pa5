
package pa5;
import java.io.IOException;
import java.util.Date;
import java.util.Observable;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * This is Observable class of this program.
 * @author Sanrasern 5710547247
 */
public class Clock extends Observable {
	
	/**Initial time.*/
	Date time;
	Date now;
	Date alarmTime;
	
	/**Initial state.*/
	State state;
	State setHour = new SetHours();
	State setMin = new SetMins();
	State setSec = new SetSecs();
	State displayTime = new  DisplayTime();
	State alarmRinging = new AlarmRinging();
	
	/**Initial variables.*/
	boolean check = true;
	private Clip clip;
	
	/**Contractor of this class.*/
	public Clock() {
		/* Set the first state when open the program. */
		state = new DisplayTime();
		now = new Date();
		alarmTime = new Date();
		time = now;
		
		/* Set the first alarm time when open the program. */
		alarmTime.setHours(0);
		alarmTime.setMinutes(0);
		alarmTime.setSeconds(0);
	}
	
	/**This is a method to update the time.*/
	public void updateTime() {
		if(isTimeEquals()){
			this.setState(this.alarmRinging);
			if(check) play();
		}
		now.setTime(System.currentTimeMillis());
		super.setChanged();
		super.notifyObservers(time);
	}
	
	/**To play the alarm sound.*/
	public void play(){
        AudioInputStream audioInputStream;
        try {
                audioInputStream = AudioSystem.getAudioInputStream(getClass().getResource("/Sound/sound.wav"));
                clip = AudioSystem.getClip();
                clip.open(audioInputStream);
                clip.start();
                clip.loop(Clip.LOOP_CONTINUOUSLY);
        } catch (UnsupportedAudioFileException e) {
                e.printStackTrace();
        } catch (IOException e) {
                e.printStackTrace();
        } catch (LineUnavailableException e) {
                e.printStackTrace();
        }
	}
	
	/**To stop the alarm sound.*/
	public void stop(){
		clip.stop();
	}
	
	/**
	 * To check that current time is equals to alarm time or not.
	 * @return true if current time is same time to alarm time.
	 */
	public boolean isTimeEquals() {
		if(now.getHours() == alarmTime.getHours() && now.getMinutes() == alarmTime.getMinutes() && now.getSeconds() == alarmTime.getSeconds()) 
			return true;
		return false;
	}
	
	/**
	 * To get the current state.
	 * @return current state.
	 */
	public State getState() {
		return this.state;
	}
	
	/**To set the state.*/
	public void setState(State state) {
		this.state = state;
	}
}
