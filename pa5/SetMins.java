package pa5;

import java.awt.Color;

public class SetMins implements State {

	@Override
	public void hitSet(GUI gui , Clock clock) {
		/* Go to setSec state. */
		clock.setState(clock.setSec);
		
		/* Set color of the text. */
		gui.min1.setForeground(Color.BLACK);
		gui.min2.setForeground(Color.BLACK);
		gui.sec1.setForeground(Color.GREEN);
		gui.sec2.setForeground(Color.GREEN);
	}

	@Override
	public void hitMinus(Clock clock) {
		/* Set to edit alarm time. */
		clock.alarmTime.setMinutes(Math.floorMod(clock.alarmTime.getMinutes() - 1 , 60  ));
		
	}

	@Override
	public void hitPlus(Clock clock) {
		/* Set to edit alarm time. */
		clock.alarmTime.setMinutes(Math.floorMod(clock.alarmTime.getMinutes() + 1 , 60 ));
		
	}


}
