package pa5;

import java.awt.Color;

/**
 *  This is a SetHours State.
 * @author Sanrasern 5710547247
 */
public class SetHours implements State {

	@Override
	public void hitSet(GUI gui , Clock clock) {
		/* Go to setMin state. */
		clock.setState(clock.setMin);
		
		/* Set color of the text. */
		gui.hour1.setForeground(Color.BLACK);
		gui.hour2.setForeground(Color.BLACK);
		gui.min1.setForeground(Color.GREEN);
		gui.min2.setForeground(Color.GREEN);
		
	}

	@Override
	public void hitMinus(Clock clock) {
		/* Set to edit alarm time. */
		clock.alarmTime.setHours(clock.alarmTime.getHours() - 1);
		
	}

	@Override
	public void hitPlus(Clock clock) {
		/* Set to edit alarm time. */
		clock.alarmTime.setHours(clock.alarmTime.getHours() + 1);
	}

	

}
