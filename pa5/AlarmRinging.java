package pa5;

/**
 * This is a AlarmRinging State.
 * @author Sanrasern 5710547247
 */
public class AlarmRinging implements State {
	
	@Override
	public void hitSet(GUI gui , Clock clock) {
		/*Go to displayTime state*/
		clock.setState(clock.displayTime);
		/*Stop the alarm sound.*/
		clock.stop();
	}

	@Override
	public void hitMinus(Clock clock) {
		/*Go to displayTime state*/
		clock.setState(clock.displayTime);
		/*Stop the alarm sound.*/
		clock.stop();
	}

	@Override
	public void hitPlus(Clock clock) {
		/*Go to displayTime state*/
		clock.setState(clock.displayTime);
		/*Stop the alarm sound.*/
		clock.stop();
	}

	

}
