package pa5;

import java.awt.Color;
/**
 * 
 * @author Sanrasern 5710547247
 *
 */
public class DisplayTime implements State {

	@Override
	public void hitSet(GUI gui, Clock clock) {
		clock.setState(clock.setHour);
		clock.time = clock.alarmTime;
		gui.hour1.setForeground(Color.GREEN);
		gui.hour2.setForeground(Color.GREEN);
		
		
	}

	@Override
	public void hitMinus(Clock clock) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hitPlus(Clock clock) {
		clock.time = clock.alarmTime;
		
	}

}
