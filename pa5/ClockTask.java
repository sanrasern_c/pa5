package pa5;

import java.util.TimerTask;
	/**
	 * To call updateTime.
	 * @author Sanrasern 5710547247
	 */
	public class ClockTask extends TimerTask {
	Clock clock;
	
	/** Constructor of this class. */
	public ClockTask(Clock clock) {
		this.clock = clock;
	}
	
	/** To call update upadateTime. */
	public void run() {
		clock.updateTime();
	}
	
}
